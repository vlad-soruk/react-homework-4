const defaultState =  {
    addModalState: false,
    deleteModalState: false
}

const OPEN_ADD_MODAL = "OPEN_ADD_MODAL";
const CLOSE_ADD_MODAL = "DELETE_ADD_MODAL";

const OPEN_DELETE_MODAL = "OPEN_DELETE_MODAL";
const CLOSE_DELETE_MODAL = "DELETE_DELETE_MODAL";

export const modalsReducer = (state=defaultState, action) => {
    switch (action.type) {
        case OPEN_ADD_MODAL:
            return {...state, addModalState: true}

        case CLOSE_ADD_MODAL:
            return {...state, addModalState: false}

        case OPEN_DELETE_MODAL:
            return {...state, deleteModalState: true}

        case CLOSE_DELETE_MODAL:
            return {...state, deleteModalState: false}

        default:
            return state
    }
} 

export const openAddModalAction = (payload) => ({type: OPEN_ADD_MODAL, payload})
export const closeAddModalAction = (payload) => ({type: CLOSE_ADD_MODAL, payload})

export const openDeleteModalAction = (payload) => ({type: OPEN_DELETE_MODAL, payload})
export const closeDeleteModalAction = (payload) => ({type: CLOSE_DELETE_MODAL, payload})