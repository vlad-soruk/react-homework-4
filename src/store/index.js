import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "@redux-devtools/extension";
import thunk from "redux-thunk";
import { productsReducer } from "./productsReducer";
import { modalsReducer } from "./modalsReducer";

const rootReducer = combineReducers({
    products: productsReducer,
    modals: modalsReducer
})

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))